import io
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.data import DataLoader


def plot_losses(train_loss, val_loss, test=False):
    plt.figure(figsize=(15, 10))

    fig, ax = plt.subplots()

    ax.plot(
        np.arange(len(train_loss)) + 1,
        train_loss,
        label="training loss",
        color="darkgreen",
    )

    ax.plot(
        np.arange(len(val_loss)) + 1,
        val_loss,
        label="test loss" if test else "validation loss",
        color="skyblue",
    )
    if test:
        ax.set_title(f"Train loss vs test loss")
    else:
        ax.set_title(f"Train loss vs val loss")
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Loss")
    ax.legend(bbox_to_anchor=[1.4, 1], loc=1)

    plt.show()
    return fig


def plot_to_image(figure):
    # Save the plot to a buffer in memory
    buf = io.BytesIO()
    figure.savefig(buf, dpi=300, format="png")
    buf.seek(0)

    # Convert the buffer to a PIL image
    image = Image.open(buf)
    # Convert the PIL image to a PyTorch tensor
    image_tensor = torch.Tensor(np.array(image)).permute(2, 0, 1).unsqueeze(0) / 255.0

    # Remove the batch dimension (the first dimension)
    image_tensor = image_tensor.squeeze(0)

    # Select only the first 3 channels (RGB)
    image_tensor = image_tensor[:3, :, :]

    buf.close()
    return image_tensor


def plot_outputs(true_values, predicted_values, n_lines=20):
    n_lines = min(n_lines, len(true_values))
    for i in range(n_lines):
        diff = true_values[i] - predicted_values[i]
        plt.plot(diff, "-x")
        plt.title("Difference between true value and predicted value")
        plt.xlabel("20 min segments")
        plt.ylabel("Height difference (m)")
    plt.show()


def plot_whole_period(model, places, datasets, device):
    with torch.no_grad():
        model.eval()
        fig, axes = plt.subplots(1, 3, figsize=(15, 5))
        for ax, name, dataset in zip(axes.flat, places, datasets):
            loader = DataLoader(dataset, batch_size=1, shuffle=False)
            predicted_hs = []
            target_hs = []

            for imgs, target in loader:
                if isinstance(imgs, list) or isinstance(imgs, tuple):
                    imgs = [im.to(device) for im in imgs]
                    pred = model(*imgs).cpu()
                else:
                    imgs = imgs.to(device)
                    pred = model(imgs).cpu()
                predicted_hs += list(pred[0].cpu().numpy())[:3]
                target_hs += list(target[0].cpu().numpy())[:3]

            ax.plot(predicted_hs, label="Predicted values")
            ax.plot(target_hs, label="True values")
            ax.set_ylabel("Significant wave height (m)")
            ax.set_title(name.capitalize())
            ax.legend()

        plt.tight_layout()
        plt.show()
    return fig


# def plot_whole_period(model, places, datasets, device):
#     with torch.no_grad():
#         model.eval()
#         fig, axes = plt.subplots(nrows=1, ncols=len(places), figsize=(15, 5))

#         for idx, (name, dataset) in enumerate(zip(places, datasets)):
#             loader = DataLoader(dataset, batch_size=1, shuffle=False)
#             predicted_hs = []
#             target_hs = []
#             for imgs, target in loader:
#                 in_1, in_2 = imgs
#                 pred = model(in_1.to(device), in_2.to(device)).cpu()
#                 predicted_hs += list(pred[0].cpu().numpy())[:3]
#                 target_hs += list(target[0].cpu().numpy())[:3]

#             axes[idx].plot(predicted_hs, label="Predicted values")
#             axes[idx].plot(target_hs, label="True values")
#             axes[idx].set_ylabel("Significant wave height (m)")
#             axes[idx].set_title(name.capitalize())
#             axes[idx].legend()

#         plt.tight_layout()
#         plt.show()


# def plot_whole_period(model, places, datasets, device):
#     with torch.no_grad():
#         model.eval()
#         for name, dataset in zip(places, datasets):
#             loader = DataLoader(dataset, batch_size=1, shuffle=False)
#             predicted_hs = []
#             target_hs = []
#             # target_hs = []
#             for imgs, target in loader:
#                 in_1, in_2 = imgs
#                 pred = model(in_1.to(device), in_2.to(device)).cpu()
#                 predicted_hs += list(pred[0].cpu().numpy())[:3]
#                 # predicted_hs += model(in_1.to(device),in_2.to(device))
#                 target_hs += list(target[0].cpu().numpy())[:3]
#                 # target_hs += list(target[3].cpu())
#             plt.plot(predicted_hs, label="Predicted values")
#             plt.plot(target_hs, label="True values")
#             plt.ylabel("Significant wave height (m)")
#             plt.title(name.capitalize())
#             plt.legend()
#             plt.show()
