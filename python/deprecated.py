def write_to_log(filename, epochs, loss_fn, optimizer, val_losses, model):
    with open(filename, "a", encoding="utf-8") as f:
        val_loss = val_losses[-1]
        min_val_loss = min(val_losses[-5:])
        out = [
            str(val_loss),
            str(min_val_loss),
            str(epochs),
            loss_fn.__str__(),
            optimizer.__str__().replace("\n", "\t"),
            model.__str__().replace("\n", "\t"),
        ]
        f.write(";".join(out) + "\n")


# predicts = []
# labels = []

# model.eval()
# with torch.no_grad():
#     for img, label in test_loader:
#         predicts += model(img).cpu()[:,0]
#         labels += label.cpu()


# predicts = [p.item() for p in predicts]
# labels = [l.item() for l in labels]

# labels = labels[0:40]
# predicts = predicts[0:40]

# w=0.25
# r=np.arange(len(labels))
# plt.bar(r,labels, label="True values",width=w)
# plt.bar(r+w,predicts, label="Predicted values",width=w)
# plt.legend()
# plt.show()
# Ignite
# from ignite.engine import Engine


# def train_step(engine,batch):
#     model.train()
#     # for imgs, labels in train_loader:  # For each batch
#     imgs, labels = batch
#     optimizer.zero_grad()  # Reset gradients for next batch

#     outputs = model(imgs)  # Make predictions

#     # Gradient descent
#     loss = nn.MSELoss(outputs, labels)
#     # loss = torch.sqrt(loss_fn(outputs[:, 0], labels))

#     loss.backward()
#     optimizer.step()

#     return loss.item()

# trainer = Engine(train_step)
# def validation_step(engine,batch):
#     model.eval()
#     with torch.no_grad():
#         x,y = batch
#         y_pred = model(x)
#     return y_pred, y

# evaluator = Engine(validation_step)
# from ignite.metrics import Accuracy

# Accuracy().attach(evaluator, "accuracy")


# Trash
# predicts = [p for p in predicts]
# labels = [l for l in labels]
# win_length = 20

# for i in range(10,30):
#     l = labels[i]
#     p = predicts[i]

#     w=0.25
#     r=np.arange(len(l))
#     plt.plot(l,".-",label="True values")
#     plt.plot(p,".-",label="Predicted values")

#     # plt.bar(r,l, label="True values",width=w)
#     # plt.bar(r+w,p, label="Predicted values",width=w)
#     plt.xticks(np.arange(num_classes),[f"{i*win_length}" for i in range(num_classes)])
#     plt.legend()
#     # plt.xticks(np.arange(num_classes)*win_length)
#     plt.show()
# def create_imgs(channels:list):
#     biggest_shape = max([c.size() for c in channels])
#     for i in range(len(channels)):
#         channels[i] = torch.nn.functional.interpolate(torch.unsqueeze(torch.unsqueeze(channels[i],0),0),biggest_shape)[0][0]
#     return torch.stack(channels,0)


# p = create_imgs([elevation_channel,temperature_channels[0],pressure_channels[0]])[1]

# import matplotlib.pyplot as plt
# elevation_channel[elevation_channel<0] = 0
# elevation_channel[(elevation_channel>0)] = 700
# plt.imshow(np.flip(elevation_channel,0))
# plt.colorbar()
# plt.show()

# def find_location_indices(lat_point, lon_point,lats,lons,):
#     lat_i = np.argmin((lats-lat_point)**2)
#     lon_j = np.argmin((lons-lon_point)**2)
#     return lat_i,lon_j


# def find_location_indices(lon_i, lat_i, lon_ref, lat_ref):
#     dist_squared = (lon_ref - lon_i)**2 + (lat_ref - lat_i)**2
#     return np.unravel_index(np.argmin(dist_squared, axis=None), dist_squared.shape)


# def get_subsection(lon_center,lat_center,lon_size,lat_size,data,lons,lats):
#     # TODO deal with coordinates wrapping around
#     lower_lon = lon_center-lon_size/2
#     upper_lon = lon_center+lon_size/2


#     lower_lat = lat_center-lat_size/2
#     upper_lat = lat_center+lat_size/2

#     lower_i, lower_j = find_location_indices(lower_lon,lower_lat,lons,lats)
#     upper_i, upper_j = find_location_indices(upper_lon,upper_lat,lons,lats)

#     return data[lower_i:upper_i,lower_j:upper_j]


# def get_subsection(lat_center,lon_center,resolution,height,width,lats,lons):
#     # TODO deal with coordinates wrapping around
#     center_i,center_j = find_location_indices(lat_center,lon_center,lats,lons)

#     h = round(height/resolution/2)

#     w = round(width/resolution/2)


#     return (center_i-h,center_j-w),(center_i+h,center_j+w)


# def get_subsection(lon_center,lat_center,resolution,width,height,data,lons,lats):
#     # TODO deal with coordinates wrapping around
#     center_i,center_j = find_location_indices(lon_center,lat_center,lons,lats)

#     h = round(height/resolution/2)

#     w = round(width/resolution/2)


#     return data[center_i-h:center_i+h,center_j-w:center_j+w]

### Create channels
# from torchvision import transforms
# def normalize_channels(channels:torch.Tensor):
#     mean = channels.mean()
#     std = channels.std()
#     transform = transforms.Normalize(mean=mean, std=std)
#     return transform(channels)

# print(f"{pressure_channels.max() = }")
# print(f"{pressure_channels.min() = }")


# norm_pressure_channels = normalize_channels(pressure_channels)
# print(f"{norm_pressure_channels.max() = }")
# print(f"{norm_pressure_channels.min() = }")

# met_data = open_dataset("met_data/adaptor.mars.internal-1675112170.6514373-9445-12-40e290e4-2ac7-402a-b969-e6dd9d611a29.nc")
# met_data["t2m"][1]

# lat = 60.523095
# lon = 5.266472
# width = 500 # km
# height = 500 # km


# elevation_dataset = open_dataset("Bathymetry/gebco_2022_n72.0_s30.0_w-45.0_e45.0.nc")
# elevation_lons,elevation_lats = elevation_dataset["lon"][:],elevation_dataset["lat"][:]

# lower_ij_elevation, upper_ij_elevation = get_subsection(lon,lat,0.5,width,height,elevation_lons,elevation_lats)
# i1,j1 = lower_ij_elevation
# i2,j2 = upper_ij_elevation

# # elevation_map = np.flip(elevation_dataset["elevation"][:],0)
# elevation_map = torch.tensor(elevation_dataset["elevation"][:])

# elevation_map = normalize_channels(torch.unsqueeze(elevation_map,0))[0]

# elevation_channel = elevation_map[j1:j2,i1:i2]

# met_data = open_dataset("met_data/adaptor.mars.internal-1675112170.6514373-9445-12-40e290e4-2ac7-402a-b969-e6dd9d611a29.nc")


# met_lons, met_lats = met_data["longitude"][:],met_data["latitude"][:]
# lower_ij_met, upper_ij_met = get_subsection(lon,lat,30,width,height,met_lons,met_lats)
# i1,j1 = lower_ij_met
# i2,j2 = upper_ij_met

# pressure_channels = normalize_channels(torch.tensor(met_data["msl"][:][:,j1:j2,i1:i2]))
# temperature_channels = normalize_channels(torch.tensor(met_data["t2m"][:][:,j1:j2,i1:i2]))

# pressure_channels.shape

# # See torch.utils.data.ConcatDataset(datasets) for combining datasets

# class MetDataset(torch.utils.data.Dataset):
#     def __init__(self, annotations_dataset:xr.Dataset, bathymetry_dataset:xr.Dataset,met_dataset:xr.Dataset,width:float,height:float, img_size:tuple = None,highest_resolution = None):
#         # TODO filter out only the parts in common with met_data and annotations_dataset

#         # TODO create annotations_dataset with h0 etc
#         """
#         Either img_size has to be provided, or the resolution (in km) of the highest resolution dataset. If both are provided, img_size takes precedence

#         """
#         if img_size is None:
#             assert highest_resolution != None, "Either img_size has to be provided, or the resolution (in km) of the highest resolution dataset."
#             img_size = (int(height//highest_resolution),int(width//highest_resolution))

#         self.img_size = img_size
#         # self.img_labels = pd.read_csv(annotations_file)

#         self.lat = 60.523095 # TODO extract from "annotations file" or pass in
#         self.lon = 5.266472

#         self.width = width
#         self.height = height

#         # self.init_normalized_elevation_channel(bathymetry_file,width,height)

#         self.elevation_dataset = bathymetry_dataset
#         self.elevation_normalizer = self.get_normalizer(torch.tensor(self.elevation_dataset["elevation"].values))


#         self.pressure_dataset = met_dataset["msl"]
#         self.pressure_normalizer = self.get_normalizer(torch.tensor(self.pressure_dataset.values))
#         self.temperature_dataset = met_dataset["t2m"]
#         self.temperature_normalizer = self.get_normalizer(torch.tensor(self.pressure_dataset.values))
#         # met_lons, met_lats = met_data["longitude"][:],met_data["latitude"][:]
#         # lower_ij_met, upper_ij_met = self.get_subsection(self.lat,self.lon,30,height,width,met_lats,met_lons)
#         # i1,j1 = lower_ij_met
#         # i2,j2 = upper_ij_met


#         # self.pressure_channels = self.normalize_channels(torch.tensor(met_data["msl"][:][:,j1:j2,i1:i2]))
#         # self.temperature_channels = self.normalize_channels(torch.tensor(met_data["t2m"][:][:,j1:j2,i1:i2]))

#         self.labels_dataset = annotations_dataset


#     # def init_normalized_elevation_channel(self,bathymetry_file,width,height):

#     #     elevation_dataset = open_dataset(bathymetry_file)
#     #     # elevation_lons,elevation_lats = elevation_dataset["lon"][:],elevation_dataset["lat"][:]
#     #     subsec = torch.tensor(self.get_subsection(elevation_dataset,self.lat,self.lon,width,height).values)
#     #     entire_map = torch.tensor(elevation_dataset["elevation"].values)
#     #     # elevation_map = self.normalize_channels(torch.unsqueeze(elevation_map,0))[0]
#     #     self.elevation_channel = self.normalize_channels(subsec,entire_map)


#         # lower_ij_elevation, upper_ij_elevation = self.get_subsection(self.lat,self.lon,0.5,height,width,elevation_lats,elevation_lons)
#         # i1,j1 = lower_ij_elevation
#         # i2,j2 = upper_ij_elevation

#     # def get_subsection(self,lat_center,lon_center,resolution,height,width,lats,lons):
#     #     # TODO deal with coordinates wrapping around
#     #     center_i,center_j = self.find_location_indices(lat_center,lon_center,lats,lons)

#     #     h = round(height/resolution/2)

#     #     w = round(width/resolution/2)


#     #     return (center_i-h,center_j-w),(center_i+h,center_j+w)

#     def get_subsection(self,dataset:xr.Dataset,lat_point:float,lon_point:float, width:float, height:float):

#         """
#         dataset:xr.Dataset Dataset to get subsection of. Must have lat and lon as labels
#         lat_point:float Center latitude
#         lon_point:float Center longitude
#         width:float Width of subsection in km
#         height:float height of subsection in km

#         """
#         earth_r = 6371 #Km
#         lon_length = np.abs(np.cos(np.pi*lat_point/180)*earth_r*2*np.pi/360)
#         lat_length = earth_r*2*np.pi/360
#         lons = width/lon_length
#         lats = height/lat_length

#         lon_slice = slice(lon_point-lons/2,lon_point+lons/2)
#         lat_slice = slice(lat_point-lats/2,lat_point+lats/2)

#         return dataset.sel(lon=lon_slice,lat=lat_slice)


#     # def find_location_indices(self,lat_point, lon_point,lats,lons,):
#     #     lat_i = np.argmin((lats-lat_point)**2)
#     #     lon_j = np.argmin((lons-lon_point)**2)
#     #     return lat_i,lon_j

#     def get_normalizer(self,dataset:torch.Tensor):
#         mean = dataset.mean()
#         std = dataset.std()
#         return transforms.Normalize(mean=mean, std=std)


#     def create_img(self,channels:list):
#         for i in range(len(channels)):
#             channels[i] = torch.nn.functional.interpolate(torch.unsqueeze(torch.unsqueeze(channels[i],0),0),self.img_size,mode="bilinear")[0][0]
#         return torch.stack(channels,0)


#     def __len__(self):
#         return self.pressure_dataset["time"].values.shape[0]

#     def __getitem__(self, idx):
#         elevation = torch.tensor(self.get_subsection(self.elevation_dataset,self.lat,self.lon,self.width,self.height)["elevation"].values)
#         pressure = torch.tensor(self.get_subsection(self.pressure_dataset,self.lat,self.lon,self.width,self.height)[idx].values)
#         temperature = torch.tensor(self.get_subsection(self.temperature_dataset,self.lat,self.lon,self.width,self.height)[idx].values)
#         image = self.create_img([elevation,temperature,pressure])
#         label = 1 # TODO (significant_wave_height, wave_period)
#         # if self.transform:
#         #     image = self.transform(image)
#         # if self.target_transform:
#         #     label = self.target_transform(label)
#         return image, label
