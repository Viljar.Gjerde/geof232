import torch
from tqdm.auto import tqdm
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
from .utilities import plot_outputs, plot_to_image, plot_whole_period
from .models import *
import gc
from torch.utils.tensorboard import SummaryWriter


def should_early_stop(losses_val, n_epochs, min_improvement=0):
    # n_epochs = require lowest val to be in the *n_epochs* last epochs of losses_val
    if n_epochs < 0:  # Negative numbers disable function
        return False
    if len(losses_val) < n_epochs + 1:
        return False
    min_last_values = min(losses_val[-n_epochs:])
    min_first_values = min(losses_val[:-n_epochs])

    return not min_last_values + min_improvement <= min_first_values


class MultiTrainer:
    def __init__(
        self,
        model_classes,
        learning_rates,
        weight_decays,
        drop_outs,
        train_loader,
        val_loader,
        batch_size,
        dataset_type,
        num_classes=9,
        device=None,
    ):
        self.train_loader = train_loader
        self.val_loader = val_loader
        self.batch_size = batch_size
        self.dataset_type = dataset_type
        self.models = []
        self.lrs = []
        self.drop_outs = []
        self.weight_decays = []
        # self.optimizers = []
        self.num_outputs = num_classes

        if device is None:
            self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        else:
            self.device = device

        for lr in learning_rates:
            for wd in weight_decays:
                for drop_out in drop_outs:
                    for m in model_classes:
                        self.lrs.append(lr)
                        self.weight_decays.append(wd)
                        self.drop_outs.append(drop_out)
                        self.models.append(m)

        self.num_models = torch.arange(len(self.models))

        self.losses_train = [[] for _ in self.num_models]
        self.losses_val = [[] for _ in self.num_models]

    def clean_memory(self):
        del self.optimizer
        del self.val_loader
        del self.train_loader
        del self.model
        gc.collect()
        torch.cuda.empty_cache()

    def loss_fn(self, y_hat, y):
        return torch.sqrt(torch.mean((y_hat - y) ** 2))

    def train(self, i, n_epochs):
        torch.manual_seed(123)
        n_batch_t = len(self.train_loader)  # number of training batches
        n_batch_v = len(self.val_loader)  # Number of validation batches
        m = self.models[i]
        if m == ConvNet:
            self.model = m(self.num_outputs, self.drop_outs[i], 256, 4)
        else:
            self.model = m(self.num_outputs, self.drop_outs[i])

        self.optimizer = torch.optim.AdamW(
            self.model.parameters(), lr=self.lrs[i], weight_decay=self.weight_decays[i]
        )
        self.optimizer.zero_grad(set_to_none=True)
        # self.optimizers.append(optimizer)
        for epoch in tqdm(range(1, n_epochs + 1)):
            # print(f"{epoch=}")
            loss_train = 0.0
            loss_val = 0.0

            for imgs, labels in self.train_loader:  # For each batch
                # Move to GPU if available
                if isinstance(imgs, list) or isinstance(imgs, tuple):
                    imgs = [im.to(device=self.device) for im in imgs]
                else:
                    imgs = imgs.to(self.device)
                labels = labels.to(device=self.device)

                self.model = self.model.to(self.device)
                self.model.train()
                # model.train()

                if isinstance(imgs, list) or isinstance(imgs, tuple):
                    # outputs = model(*imgs)
                    outputs = self.model(*imgs)
                else:
                    # outputs = model(imgs)  # Make predictions
                    outputs = self.model(imgs)  # Make predictions

                # Gradient descent
                loss = self.loss_fn(outputs, labels)
                loss.backward()

                self.optimizer.step()
                self.optimizer.zero_grad()  # Reset gradients for next batch

                loss_train += loss.item()

                # Calculate validation data
            for imgs, labels in self.val_loader:
                if isinstance(imgs, list) or isinstance(imgs, tuple):
                    imgs = [im.to(device=self.device) for im in imgs]
                else:
                    imgs = imgs.to(self.device)
                labels = labels.to(self.device)
                self.model = self.model.to(self.device)
                self.model.eval()
                # self.models[i] = self.models[i].to(self.device)
                # self.models[i].eval()

                if isinstance(imgs, list) or isinstance(imgs, tuple):
                    # outputs = self.models[i](*imgs)
                    outputs = self.model(*imgs)
                else:
                    outputs = self.model(imgs)
                    # outputs = self.models[i](imgs)

                loss = self.loss_fn(outputs, labels)

                loss_val += loss.item()

            self.losses_train[i].append(loss_train / n_batch_t)

            self.losses_val[i].append(loss_val / n_batch_v)

            if should_early_stop(self.losses_val[i], 10, 0.001):
                print("Stopping model early at epoch", epoch)
                break

        return self.losses_train, self.losses_val

    def log_results(
        self, i, experiment_type, experiment_desc, places=None, val_test_datasets=None
    ):
        train_loss = self.losses_train[i]
        val_loss = self.losses_val[i]

        metrics = {
            "training loss": np.array(train_loss[-1]),
            "validation loss": np.array(val_loss[-1]),
        }
        hparams = get_optimizer_hyperparam(self.optimizer)
        hparams["Model name"] = self.model.__str__().split("(")[0]
        hparams["Batch size"] = self.batch_size
        hparams["Experiment type"] = experiment_type
        hparams["Drop out"] = self.drop_outs[i]

        timestamp = (
            str(datetime.now()).replace(" ", "-").replace(":", "_").split(".")[0]
        )
        writer = SummaryWriter(
            f'logs/{experiment_type}/{hparams["Model name"]}lr{hparams["lr"]}_bs{self.batch_size}_{timestamp}'
        )

        for epoch in range(len(train_loss)):
            writer.add_scalar("Loss/train", train_loss[epoch], epoch)
            writer.add_scalar("Loss/validation", val_loss[epoch], epoch)
            # writer.add_scalar("Variance",variances[epoch],epoch)

        writer.add_hparams(hparams, metrics)
        sample_img, sample_label = next(iter(self.train_loader))
        # if "upscale" in experiment_type.lower():
        if isinstance(sample_img, list) or isinstance(sample_img, tuple):
            sample_img = [im.to(device=self.device) for im in sample_img]
        else:
            sample_img = sample_img.to(self.device)
        writer.add_graph(self.model, sample_img)
        # else:
        # bathy, pres_temp = sample_img
        # writer.add_graph(model,(bathy,pres_temp))
        writer.add_text("Experiment_details", experiment_desc)

        save_preds_img_to_tensorboard(
            writer,
            self.model,
            self.val_loader,
            len(train_loss),
            self.device,
            30,
        )
        if places is not None and val_test_datasets is not None:
            save_whole_period_to_tensorboard(
                writer,
                self.model,
                places,
                val_test_datasets,
                len(train_loss),
                self.device,
            )
        writer.close()

    def get_hparam(self, i):
        results = {}
        results["RMSE"] = self.losses_val[i][-1]
        results["train RMSE"] = self.losses_train[i][-1]
        results["lr"] = self.lrs[i]
        results["drop out"] = self.drop_outs[i]
        results["weight decay"] = self.weight_decays[i]
        results["model type"] = self.models[i]
        results["dataset type"] = self.dataset_type
        results["batch size"] = self.batch_size
        return results

    def get_best(self, plot=False, use_global_min=True):
        best_acc = -np.inf
        for i in self.num_models:
            if use_global_min:
                val_acc = min(
                    self.losses_val[i][3:]
                )  # Remove the first epochs in case model just got lucky
            else:
                val_acc = self.losses_val[i][-1]
            if plot:
                plot_losses(
                    self.losses_train[i],
                    self.losses_val[i],
                )
                print(f"Validation accuracy: {val_acc}")

            if val_acc < best_acc:
                results = self.get_hparam(i)

        return results


# class MultiTrainer:
#     def __init__(
#         self,
#         model_classes,
#         learning_rates,
#         weight_decays,
#         drop_outs,
#         train_loader,
#         val_loader,
#         batch_size,
#         num_classes=9,
#         device=None,
#     ):
#         self.train_loader = train_loader
#         self.val_loader = val_loader
#         self.batch_size = batch_size
#         self.models = []
#         self.lrs = []
#         self.drop_outs = []
#         self.weight_decays = []
#         self.optimizers = []

#         if device is None:
#             self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
#         else:
#             self.device = device

#         for lr in learning_rates:
#             for wd in weight_decays:
#                 for drop_out in drop_outs:
#                     for m in model_classes:
#                         self.lrs.append(lr)
#                         self.weight_decays.append(wd)
#                         torch.manual_seed(123)
#                         if m == ConvNet:
#                             self.models.append(m(num_classes, drop_out, 256, 4))
#                         else:
#                             self.models.append(m(num_classes, drop_out))

#         self.num_models = torch.arange(len(self.models))

#         for i in self.num_models:
#             optimizer = torch.optim.AdamW(self.models[i].parameters(), lr=self.lrs[i])
#             optimizer.zero_grad(set_to_none=True)
#             self.optimizers.append(optimizer)

#         self.isStopped = [False for _ in self.num_models]

#         self.losses_train = [[] for _ in self.num_models]
#         self.losses_val = [[] for _ in self.num_models]

#     def loss_fn(self, y_hat, y):
#         return torch.sqrt(torch.mean((y_hat - y) ** 2))

#     def train(self, i, n_epochs):
#         n_batch_t = len(self.train_loader)  # number of training batches
#         n_batch_v = len(self.val_loader)  # Number of validation batches

#         for epoch in tqdm(range(1, n_epochs + 1)):
#             # print(f"{epoch=}")
#             if sum(self.isStopped) == len(self.models):
#                 break
#             loss_train = [0 for _ in self.num_models]
#             loss_val = [0 for _ in self.num_models]

#             for imgs, labels in self.train_loader:  # For each batch
#                 # Move to GPU if available
#                 if isinstance(imgs, list) or isinstance(imgs, tuple):
#                     imgs = [im.to(device=self.device) for im in imgs]
#                 else:
#                     imgs = imgs.to(self.device)
#                 labels = labels.to(device=self.device)

#                 # for i in self.num_models:
#                 # print(i)
#                 if self.isStopped[i]:
#                     continue
#                 # model = self.models[i].to(self.device)
#                 self.models[i] = self.models[i].to(self.device)
#                 self.models[i].train()
#                 # model.train()

#                 if isinstance(imgs, list) or isinstance(imgs, tuple):
#                     # outputs = model(*imgs)
#                     outputs = self.models[i](*imgs)
#                 else:
#                     # outputs = model(imgs)  # Make predictions
#                     outputs = self.models[i](imgs)  # Make predictions

#                 # Gradient descent
#                 loss = self.loss_fn(outputs, labels)
#                 loss.backward()

#                 self.optimizers[i].step()
#                 self.optimizers[i].zero_grad()  # Reset gradients for next batch

#                 loss_train[i] += loss.item()

#                 # self.models[i] = model.cpu()
#                 # del model
#                 torch.cuda.empty_cache()
#                 gc.collect()

#                 # Calculate validation data
#             for imgs, labels in self.val_loader:
#                 if isinstance(imgs, list) or isinstance(imgs, tuple):
#                     imgs = [im.to(device=self.device) for im in imgs]
#                 else:
#                     imgs = imgs.to(self.device)

#                 # for i in self.num_models:
#                 if self.isStopped[i]:
#                     continue
#                 model = self.models[i].to(self.device)
#                 model.eval()
#                 # self.models[i] = self.models[i].to(self.device)
#                 # self.models[i].eval()

#                 if isinstance(imgs, list) or isinstance(imgs, tuple):
#                     # outputs = self.models[i](*imgs)
#                     outputs = model(*imgs)
#                 else:
#                     outputs = model(imgs)
#                     # outputs = self.models[i](imgs)

#                 loss = self.loss_fn(outputs, labels)

#                 loss_val[i] += loss.item()
#             self.models[i] = model.cpu()

#             # for i in self.num_models:
#             if not self.isStopped[i]:
#                 self.losses_train[i].append(loss_train[i] / n_batch_t)

#                 self.losses_val[i].append(loss_val[i] / n_batch_v)

#             if should_early_stop(self.losses_val[i], 10, 0.001):
#                 self.isStopped[i] = True
#                 print(
#                     "Stopping model early", len(self.models) - sum(self.isStopped)
#                 ), "models left"

#             # if epoch == 1 or epoch == n_epochs:
#             #     print(
#             #         f"{datetime.now().time()}  |  Epoch {epoch}  |  Training losses",
#             #         *[round(loss[-1] / n_batch_t, 5) for loss in self.losses_train],
#             #         sep="\n",
#             #     )
#             #     print(
#             #         f"{datetime.now().time()}  |  Epoch {epoch}  |  Validation losses"
#             #         * [round(loss[-1] / n_batch_t, 5) for loss in self.losses_val],
#             #         sep="\n",
#             #     )
#         return self.losses_train, self.losses_val

#     def log_results(
#         self, experiment_type, experiment_desc, places=None, val_test_datasets=None
#     ):
#         for i in self.num_models:
#             train_loss = self.losses_train[i]
#             val_loss = self.losses_val

#             metrics = {
#                 "training loss": np.array(train_loss[-1]),
#                 "validation loss": np.array(val_loss[-1]),
#             }
#             hparams = get_optimizer_hyperparam(self.optimizers[i])
#             hparams["Model name"] = self.models[i].__str__().split("(")[0]
#             hparams["Batch size"] = self.batch_size
#             hparams["Experiment type"] = experiment_type
#             hparams["Drop out"] = self.drop_outs[i]

#             timestamp = (
#                 str(datetime.now()).replace(" ", "-").replace(":", "_").split(".")[0]
#             )
#             writer = SummaryWriter(
#                 f'logs/test/{experiment_type}/{hparams["Model name"]}lr{hparams["lr"]}_bs{self.batch_size}_{timestamp}'
#             )

#             for epoch in range(len(train_loss)):
#                 writer.add_scalar("Loss/train", train_loss[epoch], epoch)
#                 writer.add_scalar("Loss/validation", val_loss[epoch], epoch)
#                 # writer.add_scalar("Variance",variances[epoch],epoch)

#             writer.add_hparams(hparams, metrics)
#             sample_img, sample_label = next(iter(self.train_loader))
#             # if "upscale" in experiment_type.lower():
#             writer.add_graph(self.models[i], sample_img)
#             # else:
#             # bathy, pres_temp = sample_img
#             # writer.add_graph(model,(bathy,pres_temp))
#             writer.add_text("Experiment_details", experiment_desc)

#             save_preds_img_to_tensorboard(
#                 writer,
#                 self.models[i],
#                 self.val_loader,
#                 len(train_loss),
#                 self.device,
#                 30,
#             )
#             if places is not None and val_test_datasets is not None:
#                 save_whole_period_to_tensorboard(
#                     writer,
#                     self.models[i],
#                     places,
#                     val_test_datasets,
#                     len(train_loss),
#                     self.device,
#                 )
#             writer.close()

#     def get_best(self):
#         best_acc = -np.inf

#         for i in self.num_models:
#             val_acc = self.losses_val[i][-1]
#             plot_losses(
#                 self.losses_train[i],
#                 self.losses_val[i],
#             )
#             print(f"Validation accuracy: {val_acc}")

#             if val_acc > best_acc:
#                 best_acc = val_acc
#                 best_train_acc = self.losses_train[i][-1]
#                 best_lr = self.lrs[i]
#                 best_drop_out = self.drop_outs[i]
#                 best_weight_decay = self.weight_decays[i]
#                 best_model_name = type(self.models[i]).split(".")[1]

#         return (
#             best_acc,
#             best_train_acc,
#             best_lr,
#             best_drop_out,
#             best_weight_decay,
#             best_model_name,
#         )


def train(
    n_epochs,
    optimizer,
    model,
    loss_fn,
    train_loader,
    val_loader,
    test=False,
    n_early_stop=-1,
):
    model_rep = model.__str__().replace("\n", "\t")
    optimizer_rep = optimizer.__str__().replace("\n", "\t")
    print(f"======== {model_rep} ========\n======== {optimizer_rep} ========")
    n_batch_t = len(train_loader)  # number of training batches
    n_batch_v = len(val_loader)  # Number of validation batches
    losses_train = []
    losses_val = []

    optimizer.zero_grad(set_to_none=True)

    for epoch in tqdm(range(1, n_epochs + 1)):
        loss_train = 0.0
        loss_val = 0.0

        model.train()
        for imgs, labels in train_loader:  # For each batch
            if isinstance(imgs, list) or isinstance(imgs, tuple):
                outputs = model(imgs[0], imgs[1])
            else:
                outputs = model(imgs)  # Make predictions

            # Gradient descent
            loss = loss_fn(outputs, labels)
            # loss = torch.abs(1-outputs/labels)
            # loss = torch.sqrt(loss_fn(outputs[:, 0], labels))

            loss.backward()
            optimizer.step()
            optimizer.zero_grad()  # Reset gradients for next batch

            loss_train += loss.item()

        model.eval()
        # Calculate validation data
        with torch.no_grad():
            for imgs, labels in val_loader:
                if isinstance(imgs, list) or isinstance(imgs, tuple):
                    outputs = model(imgs[0], imgs[1])
                else:
                    outputs = model(imgs)  # Make predictions

                loss = torch.sqrt(loss_fn(outputs, labels))

                loss_val += loss.item()

        losses_train.append(loss_train / n_batch_t)

        losses_val.append(loss_val / n_batch_v)

        if should_early_stop(
            losses_val, n_early_stop, 0.001
        ):  # Check if validation data is getting worse and stop if it is
            print(f"Stopping early at epoch {epoch}")
            print(
                f"{datetime.now().time()}  |  Epoch {epoch}  |  Training loss {(loss_train / n_batch_t):.5f}"
            )
            print(
                f'{datetime.now().time()}  |  Epoch {epoch}  |  {"Test loss" if test else "Validation loss"} {(loss_val / n_batch_v):.5f}'
            )
            break

        if epoch == 1 or epoch == n_epochs or epoch % 25 == 0:
            print(
                f"{datetime.now().time()}  |  Epoch {epoch}  |  Training loss {(loss_train / n_batch_t):.5f}"
            )
            print(
                f'{datetime.now().time()}  |  Epoch {epoch}  |  {"Validation loss" if not test else "Test loss"} {(loss_val / n_batch_v):.5f}'
            )
            plot_outputs(labels.cpu(), outputs.cpu())
    # write_to_log("log_wind.csv", n_epochs, loss_fn, optimizer, losses_val, model)
    return losses_train, losses_val


def plot_losses(train_loss, val_loss, test=False):
    plt.figure(figsize=(15, 10))

    fig, ax = plt.subplots()

    ax.plot(
        np.arange(len(train_loss)) + 1,
        train_loss,
        label="training loss",
        color="darkgreen",
    )

    ax.plot(
        np.arange(len(val_loss)) + 1,
        val_loss,
        label="test loss" if test else "validation loss",
        color="skyblue",
    )
    if test:
        ax.set_title(f"Train loss vs test loss")
    else:
        ax.set_title(f"Train loss vs val loss")
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Loss")
    ax.legend(bbox_to_anchor=[1.4, 1], loc=1)

    plt.show()


def get_optimizer_hyperparam(optimizer: torch.optim) -> dict:
    info = optimizer.__str__()
    info = info.replace(" (", "")
    info = info.replace(")", "").strip()

    lines = info.split("\n")
    lines[0] = "Optimzer: " + lines[0]
    hparam = {
        line.split()[0].strip().replace(":", ""): line.split()[-1].strip()
        for line in lines
        if len(line.split()) > 1
    }

    # del hparam["differentiable"]
    # del hparam["params"]
    # del hparam["foreach"]
    return convert_dict_to_float(hparam)


def convert_dict_to_float(in_dict):
    out_dict = in_dict.copy()
    for key, value in out_dict.items():
        try:
            out_dict[key] = float(value)
        except ValueError:
            pass
    return out_dict


def save_whole_period_to_tensorboard(writer, model, places, datasets, epoch, device):
    # fig = plt.figure()
    fig = plot_whole_period(model, places, datasets, device)
    img = plot_to_image(fig)
    writer.add_image("All in test period", img, epoch)


def save_preds_img_to_tensorboard(writer, model, loader, epoch, device, n_lines=None):
    labels = []
    predicts = []
    model.eval()
    model.to(device)
    # model.cpu()

    with torch.no_grad():
        for img, label in loader:
            # img = (img[0].cpu(),img[1].cpu())
            if isinstance(img, list) or isinstance(img, tuple):
                predicts += model(img[0], img[1]).cpu()
            else:
                predicts += model(img).cpu()

            labels += label.cpu()

    # diffs = [pred-label for pred, label in zip(predicts,labels)]
    fig = plt.figure()
    if n_lines is not None:
        plot_outputs(labels, predicts, n_lines)
    else:
        plot_outputs(labels, predicts)
    img = plot_to_image(fig)
    plt.show()
    writer.add_image("Difference plot", img, epoch)
