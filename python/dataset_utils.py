import numpy as np


import xarray as xr
from xarray import open_dataset
import cupy_xarray
from matplotlib.dates import num2date, date2num
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import torch
from torchvision import transforms
from torch.utils.tensorboard import SummaryWriter


from .utilities import plot_to_image


def get_index(dataset, timestamp):
    return np.argmin(np.abs(dataset.time.values - timestamp))


def read_ERA5(file_name):
    name_dict = {"longitude": "lon", "latitude": "lat"}
    data = open_dataset(file_name).rename(name_dict)
    data = data.isel({"time": slice(np.argmin(data.time.values), -1)})
    data = data.isel({"time": slice(0, np.argmax(data.time.values))})
    if "expver" in data.coords:
        data = data.isel({"expver": 0})
    return data.reindex(lat=list(reversed(data.lat)))


class BaseMetDataset(torch.utils.data.Dataset):
    def __init__(self, config, wave_dataset, bathy_dataset):
        self.config = config
        self.width = config["width"]
        self.height = config["height"]
        self.normalize = config["normalize"]
        self.mode = config["mode"]
        self.device = config["device"]

        # img_size: tuple = None,
        #         highest_resolution=None,
        #         normalize=True,
        #         mode="interpolate",
        #         device=torch.device("cpu"),

        if "img_size" in config.keys():
            self.img_size = config["img_size"]
        else:
            assert (
                "highest_resolution" in config.keys()
            ), "Either img_size has to be provided, or the highest_resolution (in km) of the highest resolution dataset."
            img_size = (
                int(self.height // config["highest_resolution"]),
                int(self.width // config["highest_resolution"]),
            )

        if "cuda" in self.device.type:
            self.elevation_dataset = bathy_dataset.as_cupy()

        else:
            self.elevation_dataset = bathy_dataset

        self.elevation_normalizer = self.get_normalizer(
            torch.tensor(self.elevation_dataset["elevation"].data), default_norm=False
        )

        self.labels_dataset = wave_dataset

    def get_subsection(
        self,
        dataset: xr.Dataset,
        lat_point: float,
        lon_point: float,
        width: float,
        height: float,
    ):
        """
        dataset:xr.Dataset Dataset to get subsection of. Must have lat and lon as coordinates
        lat_point:float Center latitude
        lon_point:float Center longitude
        width:float Width of subsection in km
        height:float height of subsection in km

        """
        earth_r = 6371  # Km
        lon_length = np.abs(np.cos(np.pi * lat_point / 180) * earth_r * 2 * np.pi / 360)
        lat_length = earth_r * 2 * np.pi / 360
        lons = width / lon_length
        lats = height / lat_length

        lon_slice = slice(lon_point - lons / 2, lon_point + lons / 2)
        lat_slice = slice(lat_point - lats / 2, lat_point + lats / 2)

        return dataset.sel({"lon": lon_slice, "lat": lat_slice})

    def get_normalizer(self, dataset: torch.Tensor, default_norm=True):
        if default_norm:
            mean = dataset.mean()
            std = dataset.std()
            return transforms.Normalize(mean=mean, std=std)
        abs_max = dataset.abs().max()
        return lambda x: x / abs_max

    def create_img(self, channels: list, img_size):
        for i in range(len(channels)):
            channels[i] = torch.nn.functional.interpolate(
                channels[i], img_size, mode="bilinear"
            )[0][0]
            # channels[i] = torch.nn.functional.interpolate(torch.unsqueeze(torch.unsqueeze(channels[i],0),0),self.img_size,mode="bilinear")[0][0]

        return torch.stack(channels, 0)

    def __len__(self):
        return self.labels_dataset.sizes["time"]

    def __getitem__(self, idx):
        pass


# class MetDataset(torch.utils.data.Dataset):
#     def __init__(
#         self,
#         annotations_dataset: xr.Dataset,
#         bathymetry_dataset: xr.Dataset,
#         met_dataset: xr.Dataset,
#         width: float,
#         height: float,
#         img_size: tuple = None,
#         highest_resolution=None,
#         normalize=True,
#         mode="interpolate",
#         device=torch.device("cpu"),
#     ):
#         # TODO filter out only the parts in common with met_data and annotations_dataset

#         # TODO create annotations_dataset with h0 etc
#         """
#         Either img_size has to be provided, or the resolution (in km) of the highest resolution dataset. If both are provided, img_size takes precedence

#         """
#         if img_size is None:
#             assert (
#                 highest_resolution != None
#             ), "Either img_size has to be provided, or the resolution (in km) of the highest resolution dataset."
#             img_size = (
#                 int(height // highest_resolution),
#                 int(width // highest_resolution),
#             )

#         self.img_size = img_size

#         self.width = width
#         self.height = height

#         self.normalize = normalize

#         self.mode = mode

#         self.device = device

#         if "cuda" in device.type:
#             self.elevation_dataset = bathymetry_dataset.as_cupy()

#         else:
#             self.elevation_dataset = bathymetry_dataset

#         self.elevation_normalizer = self.get_normalizer(
#             torch.tensor(self.elevation_dataset["elevation"].data), default_norm=False
#         )

#         self.labels_dataset = annotations_dataset


#     def __getitem__(self, idx):
#         pass


class Upscale_Normal_Dataset(BaseMetDataset):
    def __init__(self, config, wave_dataset, bathy_dataset, met):
        super().__init__(config, wave_dataset, bathy_dataset)

        self.pressure_dataset = met["msl"]
        self.pressure_normalizer = self.get_normalizer(
            torch.tensor(self.pressure_dataset.data)
        )
        self.temperature_dataset = met["t2m"]
        self.temperature_normalizer = self.get_normalizer(
            torch.tensor(self.temperature_dataset.data)
        )

    def show_input(self, experiment_type):
        img = self.__getitem__(0)[0]
        layers = [img[0], img[1], img[2]]
        names = ["temperature", "pressure", "elevation"]
        fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(15, 4))
        for i, ax in enumerate(axes.flat):
            im = ax.imshow(np.flip(layers[i].cpu().numpy(), 0))
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)

            plt.colorbar(im, cax=cax)
            ax.set_title(names[i])

        if experiment_type:
            writer = SummaryWriter("logs/" + experiment_type + "/")
            writer.add_image("Input Images", plot_to_image(fig))
        plt.show()

    def __getitem__(self, idx):
        label_point = self.labels_dataset.isel({"time": idx})
        lat = label_point.lat.values
        lon = label_point.lon.values
        label = torch.tensor(label_point.hs.values, device=self.device)

        elevation = torch.tensor(
            self.get_subsection(
                self.elevation_dataset, lat, lon, self.width, self.height
            )["elevation"].data,
            device=self.device,
        )

        pressure = torch.tensor(
            self.get_subsection(
                self.pressure_dataset, lat, lon, self.width, self.height
            )[idx].data,
            device=self.device,
        )
        temperature = torch.tensor(
            self.get_subsection(
                self.temperature_dataset, lat, lon, self.width, self.height
            )[idx].data,
            device=self.device,
        )
        # add dimensions to match pytorch expectations
        elevation = elevation.unsqueeze(0).unsqueeze(0)
        pressure = pressure.unsqueeze(0).unsqueeze(0)
        temperature = temperature.unsqueeze(0).unsqueeze(0)

        if self.normalize:
            elevation = self.elevation_normalizer(elevation)
            pressure = self.pressure_normalizer(pressure)
            temperature = self.temperature_normalizer(temperature)

        image = self.create_img([temperature, pressure, elevation], (512, 512))
        return image.float(), label.float()


class Split_1_2_Dataset(BaseMetDataset):
    def __init__(self, config, wave_dataset, bathy_dataset, met):
        super().__init__(config, wave_dataset, bathy_dataset)

        self.pressure_dataset = met["msl"]
        self.pressure_normalizer = self.get_normalizer(
            torch.tensor(self.pressure_dataset.data)
        )
        self.temperature_dataset = met["t2m"]
        self.temperature_normalizer = self.get_normalizer(
            torch.tensor(self.temperature_dataset.data)
        )

    def show_input(self, experiment_type):
        img = self.__getitem__(0)[0]
        bathy, temp_pres = img
        layers = [bathy[0], temp_pres[0], temp_pres[1]]
        names = ["elevation", "temperature", "pressure"]
        fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(15, 4))
        for i, ax in enumerate(axes.flat):
            im = ax.imshow(np.flip(layers[i].cpu().numpy(), 0))
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)

            plt.colorbar(im, cax=cax)
            ax.set_title(names[i])

        if experiment_type:
            writer = SummaryWriter("logs/" + experiment_type + "/")
            writer.add_image("Input Images", plot_to_image(fig))
        plt.show()

    def __getitem__(self, idx):
        label_point = self.labels_dataset.isel({"time": idx})
        lat = label_point.lat.values
        lon = label_point.lon.values
        label = torch.tensor(label_point.hs.values, device=self.device)

        elevation = torch.tensor(
            self.get_subsection(
                self.elevation_dataset, lat, lon, self.width, self.height
            )["elevation"].data,
            device=self.device,
        )

        pressure = torch.tensor(
            self.get_subsection(
                self.pressure_dataset, lat, lon, self.width, self.height
            )[idx].data,
            device=self.device,
        )
        temperature = torch.tensor(
            self.get_subsection(
                self.temperature_dataset, lat, lon, self.width, self.height
            )[idx].data,
            device=self.device,
        )
        # add dimensions to match pytorch expectations
        elevation = elevation.unsqueeze(0).unsqueeze(0)
        pressure = pressure.unsqueeze(0).unsqueeze(0)
        temperature = temperature.unsqueeze(0).unsqueeze(0)

        if self.normalize:
            elevation = self.elevation_normalizer(elevation)
            pressure = self.pressure_normalizer(pressure)
            temperature = self.temperature_normalizer(temperature)

        image = self.create_img([temperature, pressure], (48, 48))
        bathy = self.create_img([elevation], (1538, 1538))
        return (bathy.float(), image.float()), label.float()


class Split_wind_Dataset(BaseMetDataset):
    def __init__(self, config, wave_dataset, bathy_dataset, met):
        super().__init__(config, wave_dataset, bathy_dataset)
        self.u_dataset = met["u10"]
        self.u_normalizer = self.get_normalizer(torch.tensor(self.u_dataset.data))
        self.v_dataset = met["v10"]
        self.v_normalizer = self.get_normalizer(torch.tensor(self.v_dataset.data))

    def show_input(self, experiment_type):
        img = self.__getitem__(0)[0]
        bathy, winds = img
        u = winds[0]
        v = winds[1]
        u, v, bathy = u.cpu(), v.cpu(), bathy[0].cpu()
        # names = ["Wind_u", "Wind_v", "pressure"]
        fig, ax = plt.subplots(figsize=(5, 5))
        im = ax.imshow(np.flip(bathy.cpu().numpy(), 0))

        # Define the grid for the wind vectors
        x_points = np.linspace(0, bathy.shape[0], u.shape[0])
        y_points = np.linspace(0, bathy.shape[0], v.shape[0])
        X, Y = np.meshgrid(x_points, y_points)

        # Plot the wind vectors on top of the background
        ax.quiver(X, Y, u, v, scale=50, color="w", headwidth=3)

        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(im, cax=cax)
        ax.set_title("Bathymetry and wind")

        if experiment_type:
            writer = SummaryWriter("logs/" + experiment_type + "/")
            writer.add_image("Input Images", plot_to_image(fig))
        plt.show()

    def __getitem__(self, idx):
        label_point = self.labels_dataset.isel({"time": idx})
        lat = label_point.lat.values
        lon = label_point.lon.values
        label = torch.tensor(label_point.hs.values, device=self.device)

        elevation = torch.tensor(
            self.get_subsection(
                self.elevation_dataset, lat, lon, self.width, self.height
            )["elevation"].data,
            device=self.device,
        )

        wind_u = torch.tensor(
            self.get_subsection(self.u_dataset, lat, lon, self.width, self.height)[
                idx
            ].data,
            device=self.device,
        )
        wind_v = torch.tensor(
            self.get_subsection(self.v_dataset, lat, lon, self.width, self.height)[
                idx
            ].data,
            device=self.device,
        )
        # add dimensions to match pytorch expectations
        elevation = elevation.unsqueeze(0).unsqueeze(0)
        wind_u = wind_u.unsqueeze(0).unsqueeze(0)
        wind_v = wind_v.unsqueeze(0).unsqueeze(0)

        if self.normalize:
            elevation = self.elevation_normalizer(elevation)
            wind_u = self.u_normalizer(wind_u)
            wind_v = self.v_normalizer(wind_v)

        image = self.create_img([wind_u, wind_v], (48, 48))
        bathy = self.create_img([elevation], (1538, 1538))
        return (bathy.float(), image.float()), label.float()


def create_dataset(config, wave_dataset, bathy_dataset, met):
    dataset_type = config["dataset_type"]
    if dataset_type.lower() == "split_1_2":
        return Split_1_2_Dataset(config, wave_dataset, bathy_dataset, met)
    if dataset_type.lower() == "upscale_normal":
        return Upscale_Normal_Dataset(config, wave_dataset, bathy_dataset, met)
    elif dataset_type.lower() == "split_wind":
        return Split_wind_Dataset(config, wave_dataset, bathy_dataset, met)
    else:
        raise ValueError(f"Invalid dataset type: {dataset_type}")


def create_and_split_datasets(
    config,
    places,
    met_dataset,
    bathy_dataset,
    train_split,
    safety_margin,
    experiment_type,
):
    train_datasets = []
    val_test_datasets = []

    for place in places:
        wave_dataset = open_dataset(f"waves_data/multi_segment/{place}_processed.nc")
        print(len(wave_dataset.time.values))

        start_time = wave_dataset.time[0].values
        end_time = wave_dataset.time[-1].values

        met = met_dataset.isel(
            {
                "time": slice(
                    get_index(met_dataset, start_time),
                    get_index(met_dataset, end_time) + 1,
                )
            }
        ).load()  # Only extract necessary times to save memory

        temp_dataset = create_dataset(config, wave_dataset, bathy_dataset, met)

        print(len(temp_dataset))
        train_datasets.append(
            torch.utils.data.Subset(
                temp_dataset,
                range(int(len(temp_dataset) * train_split - safety_margin)),
            )
        )
        val_test_datasets.append(
            torch.utils.data.Subset(
                temp_dataset,
                range(int(len(temp_dataset) * train_split), len(temp_dataset)),
            )
        )

        temp_dataset.show_input(experiment_type)

    return train_datasets, val_test_datasets
