import torch.nn as nn
import torch


class ConvNet(nn.Module):
    #  Determine what layers and their order in CNN object

    def __init__(self, num_classes, dropout, fc_num, cnn_num):
        super().__init__()
        self.conv_layer1 = nn.Conv2d(
            in_channels=3, out_channels=cnn_num, kernel_size=75, stride=10
        )
        self.conv_act1 = nn.ReLU()
        self.max_pool1 = nn.MaxPool2d(kernel_size=4, stride=2)

        self.conv_layer2 = nn.LazyConv2d(
            out_channels=cnn_num * 2, kernel_size=11, padding="same"
        )
        self.conv_act2 = nn.ReLU()
        self.max_pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv_layer3 = nn.LazyConv2d(out_channels=cnn_num * 4, kernel_size=3)
        self.conv_act3 = nn.ReLU()
        self.max_pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.flat = nn.Flatten()

        self.fc1 = nn.LazyLinear(fc_num)
        self.act1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)
        self.fc2 = nn.LazyLinear(int(fc_num / 2))
        self.act2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)
        self.fc3 = nn.LazyLinear(int(fc_num / 4))
        self.act3 = nn.ReLU()

        self.fc4 = nn.LazyLinear(num_classes)

    # Progresses data across layers
    def forward(self, x):
        out = self.conv_layer1(x)
        out = self.conv_act1(out)
        out = self.max_pool1(out)

        out = self.conv_layer2(out)
        out = self.conv_act2(out)
        out = self.max_pool2(out)

        out = self.conv_layer3(out)
        out = self.conv_act3(out)
        out = self.max_pool3(out)

        out = self.flat(out)

        out = self.fc1(out)
        out = self.act1(out)
        out = self.dropout1(out)
        out = self.fc2(out)
        out = self.act2(out)
        out = self.dropout2(out)
        out = self.fc3(out)
        out = self.act3(out)

        out = self.fc4(out)

        return out


class SplitConvNet(nn.Module):
    #  Determine what layers and their order in CNN object

    def __init__(self, num_classes, dropout):
        super().__init__()
        self.conv_layer_bathy1 = nn.Conv2d(
            in_channels=1, out_channels=2, kernel_size=7, stride=1
        )
        self.conv_act_bathy1 = nn.ReLU()
        self.max_pool_bathy1 = nn.MaxPool2d(kernel_size=5, stride=1)

        self.conv_layer_bathy2 = nn.LazyConv2d(out_channels=2, kernel_size=5, stride=1)
        self.conv_act_bathy2 = nn.ReLU()
        self.max_pool_bathy2 = nn.MaxPool2d(kernel_size=4, stride=4)

        # self.conv_layer_bathy3 = nn.LazyConv2d(out_channels=2, kernel_size=3)
        # self.conv_act_bathy3 = nn.ReLU()
        # self.max_pool_bathy3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.flat_bathy = nn.Flatten()

        self.fc_bathy1 = nn.LazyLinear(128)
        self.act_bathy1 = nn.ReLU()

        self.fc_bathy2 = nn.LazyLinear(2304)
        self.act_bathy2 = nn.ReLU()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=4, kernel_size=3)
        self.conv_act1 = nn.ReLU()
        self.max_pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.LazyConv2d(out_channels=4, kernel_size=3)
        self.conv_act2 = nn.ReLU()
        self.max_pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.LazyConv2d(out_channels=8, kernel_size=3)
        self.conv_act3 = nn.ReLU()
        self.max_pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.flat = nn.Flatten()

        self.fc1 = nn.LazyLinear(128)
        self.act1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)
        self.fc2 = nn.LazyLinear(32)
        self.act2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)
        self.fc3 = nn.LazyLinear(16)
        self.act3 = nn.ReLU()
        # self.fc4 = nn.LazyLinear(int(fc_num/8))
        # self.act4 = nn.ReLU()
        self.fc4 = nn.LazyLinear(num_classes)

    # Progresses data across layers
    def forward(self, bathy, pres_temp):
        batch_size = bathy.shape[0]
        bathy = self.conv_layer_bathy1(bathy)
        bathy = self.conv_act_bathy1(bathy)
        bathy = self.max_pool_bathy1(bathy)

        bathy = self.conv_layer_bathy2(bathy)
        bathy = self.conv_act_bathy2(bathy)
        bathy = self.max_pool_bathy2(bathy)

        # bathy = self.conv_layer_bathy3(bathy)
        # bathy = self.conv_act_bathy3(bathy)
        # bathy = self.max_pool_bathy3(bathy)

        bathy = self.flat_bathy(bathy)

        bathy = self.fc_bathy1(bathy)
        bathy = self.act_bathy1(bathy)
        bathy = self.fc_bathy2(bathy)
        bathy = self.act_bathy2(bathy)

        bathy = torch.reshape(bathy, (batch_size, 1, 48, 48))

        out = torch.cat((bathy, pres_temp), 1)

        out = self.conv1(out)
        out = self.conv_act1(out)
        out = self.max_pool1(out)

        out = self.conv2(out)
        out = self.conv_act2(out)
        out = self.max_pool2(out)

        out = self.conv3(out)
        out = self.conv_act3(out)
        out = self.max_pool3(out)

        out = self.flat(out)

        out = self.fc1(out)
        out = self.act1(out)
        out = self.dropout1(out)
        out = self.fc2(out)
        out = self.act2(out)
        out = self.dropout2(out)
        out = self.fc3(out)
        out = self.act3(out)
        # out = self.fc4(out)
        # out = self.act4(out)
        out = self.fc4(out)

        return out


class SplitConvNet2(nn.Module):
    #  Determine what layers and their order in CNN object

    def __init__(self, num_classes, dropout):
        super().__init__()
        self.conv_layer_bathy1 = nn.Conv2d(
            in_channels=1, out_channels=2, kernel_size=3, stride=1
        )
        self.conv_act_bathy1 = nn.ReLU()
        self.max_pool_bathy1 = nn.MaxPool2d(kernel_size=4, stride=4)

        self.conv_layer_bathy2 = nn.LazyConv2d(
            out_channels=2, kernel_size=3, stride=1, padding="same"
        )
        # self.conv_layer_bathy2b = nn.LazyConv2d(out_channels=2, kernel_size=3,stride = 1,padding="same")
        self.conv_act_bathy2 = nn.ReLU()
        self.max_pool_bathy2 = nn.MaxPool2d(kernel_size=4, stride=4)

        self.conv_layer_bathy3 = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        self.conv_layer_bathy3b = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        self.conv_layer_bathy3c = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        self.conv_layer_bathy3d = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        self.conv_layer_bathy3e = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        self.conv_act_bathy3 = nn.ReLU()
        self.max_pool_bathy3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv1 = nn.Conv2d(in_channels=6, out_channels=4, kernel_size=3)
        self.conv_act1 = nn.ReLU()
        self.max_pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.LazyConv2d(out_channels=4, kernel_size=3)
        self.conv_act2 = nn.ReLU()
        self.max_pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.LazyConv2d(out_channels=8, kernel_size=3)
        self.conv_act3 = nn.ReLU()
        self.max_pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.flat = nn.Flatten()

        self.fc1 = nn.LazyLinear(512)
        self.act1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)
        self.fc2 = nn.LazyLinear(256)
        self.act2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)
        self.fc3 = nn.LazyLinear(128)
        self.act3 = nn.ReLU()
        self.fc4 = nn.LazyLinear(64)
        self.act4 = nn.ReLU()
        self.fc5 = nn.LazyLinear(32)
        self.act5 = nn.ReLU()
        self.fc6 = nn.LazyLinear(num_classes)

    # Progresses data across layers
    def forward(self, bathy, pres_temp):
        bathy = self.conv_layer_bathy1(bathy)

        bathy = self.conv_act_bathy1(bathy)
        bathy = self.max_pool_bathy1(bathy)

        bathy = self.conv_layer_bathy2(bathy)
        bathy = self.conv_act_bathy2(bathy)
        bathy = self.max_pool_bathy2(bathy)

        bathy = self.conv_layer_bathy3(bathy)
        bathy = self.conv_layer_bathy3b(bathy)
        bathy = self.conv_layer_bathy3c(bathy)
        bathy = self.conv_layer_bathy3d(bathy)
        bathy = self.conv_layer_bathy3e(bathy)
        bathy = self.conv_act_bathy3(bathy)
        bathy = self.max_pool_bathy3(bathy)

        out = torch.cat((bathy, pres_temp), 1)

        out = self.conv1(out)
        out = self.conv_act1(out)
        out = self.max_pool1(out)

        out = self.conv2(out)
        out = self.conv_act2(out)
        out = self.max_pool2(out)

        out = self.conv3(out)
        out = self.conv_act3(out)
        out = self.max_pool3(out)

        out = self.flat(out)

        out = self.fc1(out)
        out = self.act1(out)
        out = self.dropout1(out)
        out = self.fc2(out)
        out = self.act2(out)
        out = self.dropout2(out)
        out = self.fc3(out)
        out = self.act3(out)
        out = self.fc4(out)
        out = self.act4(out)
        out = self.fc5(out)
        out = self.act5(out)
        out = self.fc6(out)

        return out


class SplitConvNet1b(nn.Module):
    #  Determine what layers and their order in CNN object

    def __init__(self, num_classes, dropout):
        super().__init__()
        self.conv_layer_bathy1 = nn.Conv2d(
            in_channels=1, out_channels=2, kernel_size=3, stride=1
        )

        self.conv_act_bathy1 = nn.ReLU()
        self.max_pool_bathy1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv_layer_bathy2 = nn.LazyConv2d(
            out_channels=2, kernel_size=3, stride=1, padding="same"
        )
        # self.conv_layer_bathy2b = nn.LazyConv2d(out_channels=2, kernel_size=3,stride = 1,padding="same")
        self.conv_act_bathy2 = nn.ReLU()
        self.max_pool_bathy2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv_layer_bathy3 = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        self.conv_layer_bathy3b = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        self.conv_act_bathy3 = nn.ReLU()
        self.max_pool_bathy3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv_layer_bathy4 = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        self.conv_layer_bathy4b = nn.LazyConv2d(
            out_channels=6, kernel_size=3, padding="same"
        )
        self.conv_act_bathy4 = nn.ReLU()
        self.max_pool_bathy4 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.flat_bathy = nn.Flatten()

        self.fc_bathy1 = nn.LazyLinear(512)
        self.act_bathy1 = nn.ReLU()

        self.fc_bathy2 = nn.LazyLinear(2304)
        self.act_bathy2 = nn.ReLU()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=4, kernel_size=3)
        self.conv_act1 = nn.ReLU()
        self.max_pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.LazyConv2d(out_channels=4, kernel_size=3)
        self.conv_act2 = nn.ReLU()
        self.max_pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.LazyConv2d(out_channels=8, kernel_size=3)
        self.conv_act3 = nn.ReLU()
        self.max_pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.flat = nn.Flatten()

        self.fc1 = nn.LazyLinear(512)
        self.act1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)
        self.fc2 = nn.LazyLinear(256)
        self.act2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)
        self.fc3 = nn.LazyLinear(128)
        self.act3 = nn.ReLU()
        # self.fc4 = nn.LazyLinear(int(fc_num/8))
        # self.act4 = nn.ReLU()
        self.fc4 = nn.LazyLinear(num_classes)

    # Progresses data across layers
    def forward(self, bathy, pres_temp):
        batch_size = bathy.shape[0]
        bathy = self.conv_layer_bathy1(bathy)
        bathy = self.conv_act_bathy1(bathy)
        bathy = self.max_pool_bathy1(bathy)

        bathy = self.conv_layer_bathy2(bathy)
        bathy = self.conv_act_bathy2(bathy)
        bathy = self.max_pool_bathy2(bathy)

        bathy = self.conv_layer_bathy3(bathy)
        bathy = self.conv_layer_bathy3b(bathy)
        bathy = self.conv_act_bathy3(bathy)
        bathy = self.max_pool_bathy3(bathy)

        bathy = self.conv_layer_bathy4(bathy)
        bathy = self.conv_layer_bathy4b(bathy)
        bathy = self.conv_act_bathy4(bathy)
        bathy = self.max_pool_bathy4(bathy)

        bathy = self.flat_bathy(bathy)

        bathy = self.fc_bathy1(bathy)
        bathy = self.act_bathy1(bathy)
        bathy = self.fc_bathy2(bathy)
        bathy = self.act_bathy2(bathy)

        bathy = torch.reshape(bathy, (batch_size, 1, 48, 48))

        out = torch.cat((bathy, pres_temp), 1)

        out = self.conv1(out)
        out = self.conv_act1(out)
        out = self.max_pool1(out)

        out = self.conv2(out)
        out = self.conv_act2(out)
        out = self.max_pool2(out)

        out = self.conv3(out)
        out = self.conv_act3(out)
        out = self.max_pool3(out)

        out = self.flat(out)

        out = self.fc1(out)
        out = self.act1(out)
        out = self.dropout1(out)
        out = self.fc2(out)
        out = self.act2(out)
        out = self.dropout2(out)
        out = self.fc3(out)
        out = self.act3(out)
        # out = self.fc4(out)
        # out = self.act4(out)
        out = self.fc4(out)

        return out


class SplitConvNet3(nn.Module):
    #  Determine what layers and their order in CNN object

    def __init__(self, num_classes, dropout):
        super().__init__()

        self.actReLU = nn.ReLU()

        self.conv_layer_bathy1 = nn.Conv2d(
            in_channels=1, out_channels=2, kernel_size=3, stride=1
        )
        # self.bn_bathy1 = nn.LazyBatchNorm2d()
        self.max_pool_bathy1 = nn.MaxPool2d(kernel_size=4, stride=4)

        self.conv_layer_bathy2 = nn.LazyConv2d(
            out_channels=2, kernel_size=3, stride=1, padding="same"
        )
        # self.bn_bathy2 = nn.LazyBatchNorm2d()
        # self.conv_layer_bathy2b = nn.LazyConv2d(out_channels=2, kernel_size=3,stride = 1,padding="same")
        self.conv_act_bathy2 = nn.ReLU()
        self.max_pool_bathy2 = nn.MaxPool2d(kernel_size=4, stride=4)

        self.conv_layer_bathy3 = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        # self.bn_bathy3 = nn.LazyBatchNorm2d()
        self.conv_layer_bathy3b = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        # self.bn_bathy3b = nn.LazyBatchNorm2d()
        self.conv_layer_bathy3c = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        # self.bn_bathy3c = nn.LazyBatchNorm2d()
        self.conv_layer_bathy3d = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        # self.bn_bathy3d = nn.LazyBatchNorm2d()
        self.conv_layer_bathy3e = nn.LazyConv2d(
            out_channels=4, kernel_size=3, padding="same"
        )
        # self.bn_bathy3e = nn.LazyBatchNorm2d()
        self.rec_conv = nn.LazyConv2d(out_channels=4, kernel_size=1, padding="same")
        self.max_pool_bathy3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv1 = nn.Conv2d(in_channels=6, out_channels=64, kernel_size=3)
        # self.bn1 = nn.LazyBatchNorm2d()
        self.conv_act1 = nn.ReLU()
        self.max_pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.LazyConv2d(out_channels=128, kernel_size=3)
        self.conv_act2 = nn.ReLU()
        # self.bn2 = nn.LazyBatchNorm2d()
        # self.max_pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.LazyConv2d(out_channels=32, kernel_size=3)
        # self.bn3 = nn.LazyBatchNorm2d()
        self.conv_act3 = nn.ReLU()
        # self.max_pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.flat = nn.Flatten()

        self.fc1 = nn.LazyLinear(256)
        # self.bn_fc1 = nn.LazyBatchNorm1d()
        self.act1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)
        self.fc2 = nn.LazyLinear(128)
        # self.bn_fc2 = nn.LazyBatchNorm1d()
        self.act2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)
        self.fc3 = nn.LazyLinear(64)
        # self.bn_fc3 = nn.LazyBatchNorm1d()
        self.act3 = nn.ReLU()
        self.fc4 = nn.LazyLinear(64)
        # self.bn_fc4 = nn.LazyBatchNorm1d()
        self.act4 = nn.ReLU()
        self.fc5 = nn.LazyLinear(32)
        # self.bn_fc5 = nn.LazyBatchNorm1d()
        self.act5 = nn.ReLU()
        self.fc6 = nn.LazyLinear(num_classes)

    # Progresses data across layers
    def forward(self, bathy, pres_temp):
        bathy = self.conv_layer_bathy1(bathy)
        # bathy = self.bn_bathy1(bathy)

        bathy = self.actReLU(bathy)
        bathy = self.max_pool_bathy1(bathy)

        bathy = self.conv_layer_bathy2(bathy)
        # bathy = self.bn_bathy2(bathy)
        bathy = self.conv_act_bathy2(bathy)
        bathy = self.max_pool_bathy2(bathy)

        bathy3a = self.conv_layer_bathy3(bathy)
        # bathy = self.bn_bathy3(bathy)
        bathy = self.actReLU(bathy3a)
        bathy3a = self.rec_conv(bathy3a)

        bathy = self.conv_layer_bathy3b(bathy)
        # bathy = self.bn_bathy3b(bathy)
        bathy = self.actReLU(bathy)

        bathy = self.conv_layer_bathy3c(bathy)
        # bathy = self.bn_bathy3c(bathy)
        bathy = self.actReLU(bathy)

        bathy = self.conv_layer_bathy3d(bathy)
        # bathy = self.bn_bathy3d(bathy)
        bathy = self.actReLU(bathy)

        bathy = self.conv_layer_bathy3e(bathy + bathy3a)
        # bathy = self.bn_bathy3e(bathy)
        bathy = self.actReLU(bathy)
        bathy = self.max_pool_bathy3(bathy)

        out = torch.cat((bathy, pres_temp), 1)

        out = self.conv1(out)
        # out = self.bn1(out)
        out = self.conv_act1(out)
        out = self.max_pool1(out)

        out = self.conv2(out)
        # out = self.bn2(out)
        out = self.conv_act2(out)
        # out = self.max_pool2(out)

        out = self.conv3(out)
        # out = self.bn3(out)
        out = self.conv_act3(out)
        # out = self.max_pool3(out)

        out = self.flat(out)

        out = self.fc1(out)
        # out = self.bn_fc1(out)
        out = self.act1(out)
        out = self.dropout1(out)
        out = self.fc2(out)
        # out = self.bn_fc2(out)
        out = self.act2(out)
        out = self.dropout2(out)
        out = self.fc3(out)
        # out = self.bn_fc3(out)
        out = self.act3(out)
        out = self.fc4(out)
        # out = self.bn_fc4(out)
        out = self.act4(out)
        out = self.fc5(out)
        # out = self.bn_fc5(out)
        out = self.act5(out)
        out = self.fc6(out)

        return out
